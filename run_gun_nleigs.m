% run_gun_nleigs

timings = 0;
verbose = 1;
solver = @cork_lr;

%% nlep formulation
NLEP = gun_init;
n = NLEP.n;

%% cork linearization
[L,d,Sigma] = gun_linearization(NLEP,100,1e-11);

%% rational Krylov shifts
Z = [2/3,(1+1i)/3,0,(-1+1i)/3,-2/3].';
gam = 300^2 - 200^2; mu = 250^2;
shifts = gam*Z + mu;
shift = shifts(3);

%% nleigs starting vector
randn('state',0);
v0 = randn(9956,1); v0 = v0/norm(v0);
v0 = NLEP.Afun(Sigma(1))\v0; v0 = v0/norm(v0);

%% cork paramters
k = 21;
target = struct('Sigma',Sigma,'shift',shift);

%% cork options
m = 100;
minit = 70;
maxrest = 0;
funres = @(Lambda,X) gun_residual(Lambda,X,NLEP);
tolres = 1e-10;
opts = struct('m',m,'minit',minit,'maxrest',maxrest,'v0',v0,'funres',funres,...
    'tolres',tolres,'shifts',shifts,'verbose',verbose);

%% solve nlep
if timings
    t0 = tic; lambda = solver(L,k,target,opts); toc(t0);
else
    t0 = tic; [X,lambda,res,flag,info] = solver(L,k,target,opts); toc(t0);
    % plot
    figure; plot_conv_eig(info.Res,info.Lam,info.J,tolres);
    ylim([1e-18,1e0]);
    title(['gun problem with low rank',' (d = ',num2str(d),')']);
end
