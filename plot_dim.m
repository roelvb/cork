function plot_dim(J,R,m,d)

Jk = length(J);
Rk = length(R);
k = max([Jk,Rk]);

plot((0:Jk-1)',J,'o:k'); hold on;
plot((0:Rk-1)',R,'*:b');
if isempty(d)
    plot(m:k,(m+1):(k+1),'--m');
else
    plot([0,k-1],[d+m,d+m],'--m');
end

if k < m
    legend('j','r','Location','NorthWest');
elseif isempty(d)
    legend('j','r','i+1','Location','NorthWest');
else
    legend('j','r','m+d','Location','NorthWest');
end

if ~isempty(d), title(['d = ',num2str(d)]); end
xlabel('iteration');
ylabel('dimension');

end
