function [L,d,Sigma,Xi] = gun_linearization(NLEP,maxdgr,tollin)

%% default inputs
if nargin < 2, maxdgr = 100; end
if nargin < 3, tollin = 1e-12; end

%% target set
gam = 300^2 - 200^2; mu = 250^2;
xmin = gam*(-1) + mu;
xmax = gam*1 + mu;
npts = 1e4;
nl = floor(2*mu / ((2*mu + pi*mu)/npts));
nc = npts - nl;
line = linspace(xmin,xmax,nl+1);
halfcircle = xmin+(xmax-xmin)*(exp(1i*linspace(0,pi,nc+1))/2+.5);
Sigma = [xmax,halfcircle(2:end-1),line(1:end-1)];

%% define the set of pole candidates
Xi = -logspace(-8,8,npts) + NLEP.sigma2^2;

%% cork linearization
[L,d] = newtonapprox(NLEP,Sigma,Xi,maxdgr,tollin);

end
