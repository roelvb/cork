function varargout = cork_lr(varargin)
% CORK_LR  Find a few eigenvalues and eigenvectors of a linearization pencil
%   lambda = CORK_LR(L) returns a vector of the 6 smalest magnitude eigenvalues
%   of the low rank linearization pencil L, represented by the following
%   structure:
%
%     L.A:     cell array of (n x n) or (n x r) matrices of length d
%     L.B:     cell array of (n x n) or (n x r) matrices of length d
%     L.M:     (d-1) x d matrix
%     L.N:     (d-1) x d matrix
%     L.U:     n x r matrix
%     L.p:     positive integer
%     L.Pfun:  function handle for P(lam) [optional]
%
%           [   A_0  ...  A_p  | A_{p+1} ... A_{d-1} ]
%           [------------------|---------------------]
%     L  =  [ kron(M11,eye(n)) |          0          ]
%           [------------------|---------------------]
%           [   kron(M21,U')   |   kron(M22,eye(r))  ]
%
%                                     [   B_0  ...  B_p  | B_{p+1} ... B_{d-1} ]
%                                     [------------------|---------------------]
%                             - lam * [ kron(N11,eye(n)) |          0          ]
%                                     [------------------|---------------------]
%                                     [   kron(N21,U')   |   kron(N22,eye(r))  ]
%
%   [X,lambda,res] = CORK_LR(L) returns a vector lambda of the 6 smalest
%   magnitude eigenvalues,a matrix X whose columns are the corresponding
%   eigenvectors, and a vector res containing the residuals.
%
%   [X,lambda,res,flag] = CORK_LR(L) also returns a convergence flag. If flag is
%   0 then all the eigenvalues converged; otherwise not all converged.
%
%   CORK_LR(L,k) returns the k largest magnitude eigenvalues.
%
%   CORK_LR(L,k,sigma) returns the k eigenvalues closest to the real or complex
%   scalar sigma.
%
%   CORK_LR(L,k,target) returns the eigenvalues inside the target set
%   target.Sigma and closest to the shift target.sigma:
%     target.Sigma:  vector containing the points of a polygonal target set in
%                    the complex plane
%     target.shift:  scalar shift
%   k is a guess of the number of eigenvalues inside the target set.
%
%   CORK_LR(L,k,sigma,opts) sets the algorithm's parameters to the values in the
%   structure opts:
%     opts.m:        maximum number of ritz values
%                    [ positive integer {max(3*k,20)} ]
%     opts.p:        number of restarted ritz values
%                    [ positive integer {max(2*k,10)} ]
%     opts.minit:    min number of iterations
%                    [ positive integer {m} ]
%     opts.maxrest:  maximum number or restarts
%                    [ positive integer {50} ]
%     opts.v0:       starting vector
%                    [ vector {randomly generated} ]
%     opts.funres:   function handle for residual (Lambda[vector], X[matrix])
%                    [ @(Lambda,X) {estimated residual norm} ]
%     opts.tolres:   tolerance for residual
%                    [ positive scalar {100*eps} ]
%     opts.tolrnk:   tolerance for truncating rank
%                    [ positive scalar {[]} ]
%     opts.tollck:   tolerance for locking
%                    [ positive scalar {eps} ]
%     opts.shifts:   cyclically repeated shifts for the rational Krylov process
%                    [ vector {sigma} ]
%     opts.reuselu:  reuse of LU-factorizations
%                    [ {true} | false ]
%     opts.verbose:  level of display
%                    [ {0} | 1 | 2 ]
%
%   [X,lambda,res,flag,info] = CORK(L) also returns a structure info:
%     info.Lam:  matrix with Ritz values in each iteration 
%     info.Res:  matrix with residuals in each iteraion
%     info.J:    vector with dimension of Krylov subspace in each iteration
%     info.R1:   vector with rank of subspace Q1 in each iteration
%     info.R2:   vector with rank of subspace Q2 in each iteration
%
%   See also CORK, EIGS.

%   Reference:
%   R. Van Beeumen, K. Meerbergen, and W. Michiels. Compact rational Krylov
%   methods for nonlinear eigenvalue problems. SIAM J. Matrix Anal. Appl. 36(2),
%   pp. 820-838, 2015.
%
%   author:  Roel Van Beeumen
%   version: October 3, 2018

%% check inputs
[L,n,d,d1,d2,k,m,p,minit,maxrest,sigma,Sigma,v0,funres,tolres,tolrnk,tollck,...
    shifts,reuselu,maxreorth,resfreq,verbose,info] = ...
    checkInputs(nargout,varargin{:});

%% initialization
[Q1,Q2,U1,U2,r1,r2,j,H,K,X,lambda,res,Lam,Res,J,R1,R2,LU,SHIFTS,NNZ] = ...
    initialize(v0);

%% CORK loop
i = 1; nbrest = 0; l = 0; count = 0; flag = true;
while i <= m + maxrest*(m-p)
    
    % output
    out = nargout > 4 || (i >= minit && mod(i,resfreq) == 0) ...
                      || (i == m + maxrest*(m-p));
    
    % CORK step
    [r1,r2] = corkstep(shifts(i),r1,r2,j,i);
    
    % compute Ritz pairs and residuals
    if out, flag = ritzpairs(r1,j,i,l); elseif verbose > 1, fprintf('\n'); end
    
    % check for convergence
    if ~flag, break, end
    
    % restart
    if j == m
        if maxrest > 0 && nbrest < maxrest
            nbrest = nbrest + 1;
            [r1,r2,j,l] = implicitrestart(r1,r2,nbrest);
        else
            break
        end
    end
    
    % increment i & j
    i = i + 1;
    j = j + 1;
    
end

%% outputs
varargout = outputs(nargout,flag,i);


% ---------------------------------------------------------------------------- %
% Nested functions
% ---------------------------------------------------------------------------- %

% checkInputs: error checks the inputs to CORK and also derives some variables
% '''''''''''' from them:
%
%   L          linearization pencil
%   n          block size of L
%   d          number of block rows of L
%   d1         number of full rank block rows of L
%   d2         number of low rank block rows of L
%   k          number of eigenvalues to be computed [ 6 ]
%   m          maximum number of ritz values [ 30 ]
%   p          number of restarted ritz values [ 20 ]
%   minit      minimum number of iterations [ m ]
%   maxrest    maximum number of restarts [ 50 ]
%   sigma      scalar for finding the eigenvalues closest to [ 0 ]
%   Sigma      vector containing the points of a polygonal target set [ ]
%   v0         starting vector
%   funres     function handle for residual R(Lambda,X)
%   tolres     tolerance for residual
%   tolrnk     tolerance for truncating rank
%   tollck     tolerance for locking
%   shifts     cyclically repeated shifts for the rational Krylov process
%   reuselu    positive integer for reuse of LU-factorizations of A(sigma)
%   maxreorth  maximum number of reorthogonalizations [ 2 ]
%   resfreq    frequency for computing residuals [ 5 ]
%   verbose    level of display [ {0} | 1 | 2 ]
%   info       [ true | {false} ]
    function [L,n,d,d1,d2,k,m,p,minit,maxrest,sigma,Sigma,v0,funres,tolres,...
            tolrnk,tollck,shifts,reuselu,maxreorth,resfreq,verbose,info] = ...
            checkInputs(narg,varargin)
        
        %% linearization pencil
        L = varargin{1};
        n = size(L.A{1},1);
        d = length(L.A);
        d1 = L.p + 1;
        d2 = d - d1;
        
        %% number of eigenvalues
        if length(varargin) > 1
            k = varargin{2};
        else
            k = 6;
        end
        
        %% eigenvalues in Sigma and closest to sigma
        Sigma = [];
        if length(varargin) > 2
            if ~isstruct(varargin{3})
                sigma = varargin{3};
            else
                sigma = varargin{3}.shift;
                Sigma = varargin{3}.Sigma;
            end
        else
            sigma = 0;
        end
        
        %% set defaults
        m = max(3*k,20);
        p = max(2*k,10);
        minit = m;
        maxrest = 50;
        funres = [];
        tolres = 100*eps;
        tolrnk = [];
        tollck = eps;
        v0 = randn(n,1);
        shifts = sigma;
        reuselu = true;
        maxreorth = 2;
        if narg > 4, resfreq = 1; else, resfreq = 5; end
        verbose = 0;
        info = narg > 4;
        
        %% process the input opts
        if length(varargin) > 3
            opts = varargin{4};
            if ~isa(opts,'struct')
                error('The input argument ''opts'' must be a struct.');
            end
            if isfield(opts,'m') && ~isempty(opts.m)
                m = opts.m;
                minit = m;
            end
            if isfield(opts,'p') && ~isempty(opts.p)
                p = opts.p;
            end
            if isfield(opts,'minit') && ~isempty(opts.minit)
                minit = opts.minit;
            end
            if isfield(opts,'maxrest') && ~isempty(opts.maxrest)
                maxrest = opts.maxrest;
            end
            if isfield(opts,'v0') && ~isempty(opts.v0)
                v0 = opts.v0;
            end
            if isfield(opts,'funres') && ~isempty(opts.funres)
                funres = opts.funres;
            end
            if isfield(opts,'tolres') && ~isempty(opts.tolres)
                tolres = opts.tolres;
            end
            if isfield(opts,'tolrnk') && ~isempty(opts.tolrnk)
                tolrnk = opts.tolrnk;
            end
            if isfield(opts,'tollck') && ~isempty(opts.tollck)
                tollck = opts.tollck;
            end
            if isfield(opts,'shifts') && ~isempty(opts.shifts)
                shifts = opts.shifts;
            end
            if isfield(opts,'reuselu') && ~isempty(opts.reuselu)
                reuselu = opts.reuselu;
            end
            if isfield(opts,'verbose') && ~isempty(opts.verbose)
                verbose = opts.verbose;
            end
        end
        if isscalar(shifts)
            reuselu = true;
        end
        nb = ceil( (m + maxrest*(m-p) + 1) / floor(length(shifts)) );
        shifts = repmat(shifts(:),nb,1);
        
    end % checkInputs


    function [Q1,Q2,U1,U2,r1,r2,j,H,K,X,lambda,res,Lam,Res,J,R1,R2,LU,SHIFTS,...
            NNZ] = initialize(v0)
        
        %% matrix Q
        Q1 = v0/norm(v0);
        Q2 = [];
        r1 = size(Q1,2);
        r2 = size(Q2,2);
        
        %% tensor U
        U1 = zeros(r1,m+1,d1);
        U2 = zeros(r2,m+1,d2);
        U1(1,1,1) = 1;
        j = 1;
        
        %% Hessenberg matrices H and K
        H = zeros(m+1,m);
        K = zeros(m+1,m);
        
        %% eigenpairs and residuals
        X = nan(n,m);
        lambda = nan(m,1);
        res = nan(m,1);
        
        %% info variables
        if info
            Lam = nan(m,m + maxrest*(m-p));
            Res = nan(m,m + maxrest*(m-p));
        else
            Lam = [];
            Res = [];
        end
        J = [1;zeros(m + maxrest*(m-p),1)];
        R1 = [r1;zeros(m + maxrest*(m-p),1)];
        R2 = [r2;zeros(m + maxrest*(m-p),1)];
        
        %% lu variables
        LU = cell(1); SHIFTS = [];
        NNZ = struct('A',cellfun(@(x) nnz(x) > 0,L.A),...
            'B',cellfun(@(x) nnz(x) > 0,L.B));
        
    end % initialize


    function [rnew1,rnew2] = corkstep(shift,r1,r2,j,i)
        
        if (verbose > 0 && out) || verbose > 1, fprintf('iteration %i:',i); end
        eta = 1/sqrt(2); % reorthogonalization constant
        
        %% compute next vector
        if i == 1 || shift == K(j,j-1)/H(j,j-1)
            t = flipud(eye(j,1));
            vj1 = Q1(:,1:r1)*reshape(U1(1:r1,j,1:d1),r1,[]);
            vj2 = Q2(:,1:r2)*reshape(U2(1:r2,j,1:d2),r2,[]);
        else
            [Qj,~] = qr(K(1:j,1:j-1) - shift*H(1:j,1:j-1));
            t = Qj(:,end);
            idx = abs(t) > eps;
            vj1 = Q1(:,1:r1) * ...
                (reshape(U1(1:r1,idx,1:d1),r1,[])*kron(speye(d1),t(idx)));
            vj2 = Q2(:,1:r2) * ...
                (reshape(U2(1:r2,idx,1:d2),r2,[])*kron(speye(d2),t(idx)));
        end

        if verbose > 1, tstart = tic; end
        [v11,v2,MsN11inv10,MsN11invN1] = backslash(shift,vj1,vj2);
        if verbose > 1, telapsed = toc(tstart);
            fprintf('\n    system solve: %f seconds',telapsed);
        end
        
        %% level 1 orthogonalization
        q1 = v11;
        q2 = v2(:,1);
        delta1 = inf; nborth = 0;
        while norm(q1) < eta*delta1 && nborth <= maxreorth
            delta1 = norm(q1);
            if nborth == 0
                u11 = Q1(:,1:r1)'*v11;
                q1 = q1 - Q1(:,1:r1)*u11;
            else
                q1 = q1 - Q1(:,1:r1)*(Q1(:,1:r1)'*q1);
            end
            nborth = nborth + 1;
            if verbose > 1 && nborth > 1
                fprintf('\n    reorthogonalization Q1');
            end
        end
        if ~isempty(Q2)
            delta2 = inf; nborth = 0;
            while norm(q2) < eta*delta2 && nborth <= maxreorth
                delta2 = norm(q2);
                q2 = q2 - Q2(:,1:r2)*(Q2(:,1:r2)'*q2);
                nborth = nborth + 1;
                if verbose > 1 && nborth > 1
                    fprintf('\n    reorthogonalization Q2');
                end
            end
        end
        delta1 = norm(q1);
        delta2 = norm(q2);
        % update Q
        if delta1 > eps
            rnew1 = r1 + 1;
            q1 = q1/delta1;
            Q1(:,rnew1) = q1;
        else
            rnew1 = r1;
        end
        if delta2 > eps
            rnew2 = r2 + 1;
            q2 = q2/delta2;
            Q2(:,rnew2) = q2;
        else
            rnew2 = r2;
        end
        
        %% level 2 orthogonalization
        if rnew1 > r1
            U1(rnew1,:,:) = 0;
            u11 = [u11;q1'*v11];
        end
        if rnew2 > r2
            U2(rnew2,:,:) = 0;
        end
        if i == 1 || shift == K(j,j-1)/H(j,j-1)
            u12 = reshape(U1(1:rnew1,j,1:d1),rnew1,[]);
        else
            u12 = reshape(U1(1:rnew1,idx,1:d1),rnew1,[])*kron(speye(d1),t(idx));
        end
        u12 = u12*MsN11invN1.' - bsxfun(@times,MsN11inv10.',u11);
        u1 = [u11;reshape(u12,[],1)];
        u2 = reshape(Q2(:,1:rnew2)'*v2,[],1);
        h1 = reshape(conj(permute(U1(1:rnew1,1:j,1:d1),[2,1,3])),...
            [],d1*rnew1)*u1;
        h2 = reshape(conj(permute(U2(1:rnew2,1:j,1:d2),[2,1,3])),...
            [],d2*rnew2)*u2;
        H(1:j,j) = h1 + h2;
        K(1:j,j) = shift*H(1:j,j) + t;
        u1 = u1 - reshape(permute(U1(1:rnew1,1:j,1:d1),[1,3,2]),[],j)*H(1:j,j);
        u2 = u2 - reshape(permute(U2(1:rnew2,1:j,1:d2),[1,3,2]),[],j)*H(1:j,j);
        H(j+1,j) = norm([u1;u2]);
        K(j+1,j) = shift*H(j+1,j);
        % update U
        u1 = u1/H(j+1,j);
        u2 = u2/H(j+1,j);
        U1(1:rnew1,j+1,1:d1) = reshape(u1,[],d1);
        U2(1:rnew2,j+1,1:d2) = reshape(u2,[],d2);
        
        %% dimensions
        J(i+1) = j+1;
        R1(i+1) = rnew1;
        R2(i+1) = rnew2;
        
    end % corkstep


    function [x11,x2,MsN11inv10,MsN11invN1] = backslash(shift,y1,y2)
        
        %% check for new shift
        if ~reuselu
            newlu = true;
        else
            newlu = false;
            if isempty(SHIFTS)
                idx = 1;
                SHIFTS(idx) = shift;
                newlu = true;
            else
                idx = find(SHIFTS == shift,1,'first');
                if isempty(idx)
                    idx = length(SHIFTS) + 1;
                    SHIFTS(idx) = shift;
                    newlu = true;
                end
            end
        end
        
        %% initialize
        m10 = L.M(1:L.p,1);
        m20 = L.M(L.p+1:d-1,1);
        M11 = L.M(1:L.p,2:L.p+1);
        M21 = L.M(L.p+1:d-1,2:L.p+1);
        M22 = L.M(L.p+1:d-1,L.p+2:d);
        n10 = L.N(1:L.p,1);
        n20 = L.N(L.p+1:d-1,1);
        N11 = L.N(1:L.p,2:L.p+1);
        % N12 = L.N(1:L.p,L.p+2:d); % zero by assumption
        N21 = L.N(L.p+1:d-1,2:L.p+1);
        N22 = L.N(L.p+1:d-1,L.p+2:d);
        N1 = [n10 N11];
        N2 = [n20 N21];
        
        %% compute kronecker coefficients
        msn10 = m10 - shift*n10;
        msn20 = m20 - shift*n20;
        MsN11 = M11 - shift*N11;
        MsN21 = M21 - shift*N21;
        MsN22 = M22 - shift*N22;
        MsN11inv10 = MsN11\msn10;
        MsN11invN1 = MsN11\N1;
        MsN22invN22 = MsN22\N22;
        MsN22msum = (MsN22\MsN21)*(MsN11\msn10) - MsN22\msn20;
        MsN22Nsum = MsN22\N2 - (MsN22\MsN21)*(MsN11\N1);
        
        %% build At
        if newlu
            if isfield(L,'Pfun')
                At = L.Pfun(shift);
            else
                At = L.A{1} - shift*L.B{1};
                for ii = 2:d1
                    At = At - MsN11inv10(ii-1)*(L.A{ii} - shift*L.B{ii});
                end
                for ii = 1:d2
                    At = At + ...
                        MsN22msum(ii)*(L.A{d1+ii} - shift*L.B{d1+ii})*L.U';
                end
            end
        end
        
        %% intermediate x1 and x2 used in yt
        x1 = y1*MsN11invN1.';
        if isempty(y2)
            x2 = L.U'*y1*MsN22Nsum.';
        else
            x2 = L.U'*y1*MsN22Nsum.' + y2*MsN22invN22.';
        end
        
        %% build yt
        % B*y
        yt = L.B{1}*y1(:,1);
        for ii = 2:d1
            if NNZ.B(ii)
                yt = yt + L.B{ii}*y1(:,ii);
            end
        end
        if ~isempty(y2)
            for ii = 1:d2
                if NNZ.B(d1+ii)
                    yt = yt + L.B{d1+ii}*y2(:,ii);
                end
            end
        end
        % A1 - sigma*B1
        for ii = 2:d1
            if NNZ.A(ii) && NNZ.B(ii)
                yt = yt - L.A{ii}*x1(:,ii-1) - shift*(L.B{ii}*x1(:,ii-1));
            elseif NNZ.A(ii)
                yt = yt - L.A{ii}*x1(:,ii-1);
            elseif NNZ.B(ii)
                yt = yt - shift*(L.B{ii}*x1(:,ii-1));
            end
        end
        % A2 - sigma*B2
        for ii = 1:d2
            if NNZ.A(d1+ii) && NNZ.B(d1+ii)
                yt = yt - L.A{d1+ii}*x2(:,ii) - shift*(L.B{d1+ii}*x2(:,ii));
            elseif NNZ.A(d1+ii)
                yt = yt - L.A{d1+ii}*x2(:,ii);
            elseif NNZ.B(d1+ii)
                yt = yt - shift*(L.B{d1+ii}*x2(:,ii));
            end
        end
        
        %% compute x11 = At\yt
        if ~reuselu
            x11 = At\yt;
        else
            if newlu
                if issparse(At)
                    [AL,AU,AP,AQ,AR] = lu(At);
                    x11 = AQ * (AU \ (AL \ (AP * (AR \ yt))));
                    % improve accuracy
                    resid = yt - At*x11;
                    err = AQ * (AU \ (AL \ (AP * (AR \ resid))));
                    x11 = x11 + err;
                    LU{idx} = struct('A',At,'L',AL,'U',AU,'P',AP,'Q',AQ,'R',AR);
                else
                    [AL,AU,Ap] = lu(At,'vector');
                    x11 = AU \ (AL \ yt(Ap,:));
                    % improve accuracy
                    resid = yt - At*x11;
                    err = AU \ (AL \ resid(Ap,:));
                    x11 = x11 + err;
                    LU{idx} = struct('A',At,'L',AL,'U',AU,'p',Ap);
                end
            else
                if isfield(LU{idx},'R')
                    x11 = LU{idx}.Q * (LU{idx}.U \ (LU{idx}.L \ ...
                        (LU{idx}.P * (LU{idx}.R \ yt))));
                    % impove accuracy
                    resid = yt - LU{idx}.A*x11;
                    err = LU{idx}.Q * (LU{idx}.U \ (LU{idx}.L \ ...
                        (LU{idx}.P * (LU{idx}.R \ resid))));
                    x11 = x11 + err;
                else
                    x11 = LU{idx}.U \ (LU{idx}.L \ yt(LU{idx}.p,:));
                    % improve accuracy
                    resid = yt - LU{idx}.A*x11;
                    err = LU{idx}.U \ (LU{idx}.L \ resid(LU{idx}.p,:));
                    x11 = x11 + err;
                end
            end
        end
        
        %% compute x2
        x2 = x2 + bsxfun(@times,MsN22msum.',L.U'*x11);
        
    end % backslash


    function flag = ritzpairs(r1,j,i,l)
        
        %% QZ factorization
        qzreal = isreal(sigma) && isreal(K) && isreal(H);
        if qzreal
            [G,F,~,~,S,~] = qz(K(1:j,1:j),H(1:j,1:j),'real');
        else
            [G,F,~,~,S,~] = qz(K(1:j,1:j),H(1:j,1:j));
        end
        
        %% ritz pairs
        ordlam = ordeig(G,F);
        lambda(l+1:j) = ordlam(l+1:j);
        [idx,in] = sortlam(lambda(1:j));
        if qzreal
            ii = l+1;
            while ii <= j
                if isreal(lambda(ii))
                    S(:,ii) = S(:,ii)/norm(S(:,ii));
                    ii = ii + 1;
                else
                    Sr = S(:,ii);
                    Si = S(:,ii+1);
                    S(:,ii) = (Sr + 1i*Si)/norm(Sr + 1i*Si);
                    S(:,ii+1) = (Sr - 1i*Si)/norm(Sr - 1i*Si);
                    ii = ii + 2;
                end
            end
        else
            for ii = l+1:j
                S(:,ii) = S(:,ii)/norm(S(:,ii));
            end
        end
        X(:,l+1:j) = Q1(:,1:r1)*(U1(1:r1,1:j+1,1)*(H(1:j+1,1:j)*S(:,l+1:j)));
        for ii = l+1:j
            X(:,ii) = X(:,ii)/norm(X(:,ii));
        end
        
        %% residuals
        if isempty(funres)
            res(l+1:j) = abs((K(j+1,j) - lambda(l+1:j)*H(j+1,j)).*S(j,l+1:j).');
        else
            res(l+1:end) = nan;
            in2 = in; in2(1:l) = false;
            res(in2) = funres(lambda(in2),X(:,in2));
        end
        
        %% info
        if info
            Lam(1:j,i) = lambda(1:j);
            Res(1:j,i) = res(1:j);
        end
        
        %% check for convergence
        if j < k
            count = 0;
        elseif isempty(Sigma)
            if all(res(idx(1:k)) <= tolres)
                count = count + 1;
            else
                count = 0;
            end
        else
            if all(res(in) <= tolres)
                count = count + 1;
            else
                count = 0;
            end
        end
        flag = true;
        if i >= minit
            if (resfreq == 1 && count >= 5) || (resfreq > 1 && count > 1)
                flag = false;
            end
        end
        
        if verbose > 0 && out
            if verbose > 1, fprintf('\n    eigenvalues:'); end
            if isempty(Sigma)
                fprintf(' %i < %.2e\n',sum(res(idx(1:min(j,k))) <= tolres),...
                    tolres);
            else
               fprintf(' %i (of %i) < %.2e\n',...
                   sum(res(in) <= tolres),sum(in),tolres);
            end
            if ~flag
                if verbose > 1, fprintf('\n'); end
                if isempty(Sigma)
                    fprintf(' ==> %i converged Ritz pairs\n',k);
                else
                    fprintf([' ==> all Ritz pairs inside target set ',...
                        'converged\n']);
                end
                if verbose > 1, fprintf('\n'); end
            end
        end
        
        %% convergence
        if ~flag
            if isempty(Sigma)
                lambda = lambda(idx(1:k));
                X = X(:,idx(1:k));
                res = res(idx(1:k));
            else
                lambda = lambda(in);
                X = X(:,in);
                res = res(in);
            end
        end
        
    end % ritzpairs


    function [idx,in] = sortlam(lambda)
        
        if isempty(Sigma)
            [~,idx] = sort(abs(lambda - sigma));
            in = true(length(lambda),1);
        else
            in = inpolygon(real(lambda),imag(lambda),real(Sigma),imag(Sigma));
            Ilambda = (1:length(lambda)).';
            Ilamin = Ilambda(in);
            Ilamout = Ilambda(~in);
            [~,Iin] = sort(abs(lambda(in) - sigma));
            [~,Iout] = sort(abs(lambda(~in) - sigma));
            idx = [Ilamin(Iin);Ilamout(Iout)];
        end
        
    end % sortlam


    function [rnew1,rnew2,j,lnew] = implicitrestart(r1,r2,nbrest)
        
        %% QZ factorization
        qzreal = isreal(sigma) && isreal(K) && isreal(H);
        if qzreal
            [G,F,Y,Z] = qz(K(1:m,:),H(1:m,:),'real');
        else
            [G,F,Y,Z] = qz(K(1:m,:),H(1:m,:));
        end
        
        %% select ritz values and reorder QZ factorization
        Ilambda = sortlam(lambda);
        if qzreal && ~isreal(lambda(Ilambda(p))) && ...
                imag(lambda(Ilambda(p))) == -imag(lambda(Ilambda(p+1)))
            % increment p in case of complex conjugated ritz value
            j = p + 1;
        else
            j = p;
        end
        selres = zeros(m,1);
        [~,Ires] = sort(res);
        selres(Ires) = m:-1:1;
        selres(res == 0) = 2*m;
        select = zeros(m,1);
        if qzreal
            ii = 0;
            while ii < j
                if isreal(lambda(Ilambda(ii+1)))
                    select(Ilambda(ii+1)) = selres(Ilambda(ii+1));
                    ii = ii + 1;
                else
                    select(Ilambda(ii+1:ii+2)) = selres(Ilambda(ii+1));
                    ii = ii + 2;
                end
            end
        else
            select(Ilambda(1:j)) = selres(Ilambda(1:j));
        end
        [G,F,Y,Z] = ordqz(G,F,Y,Z,select);
        
        %% new Hessenberg matrices H and K
        Hj1 = H(m+1,:)*Z(:,1:j);
        Kj1 = K(m+1,:)*Z(:,1:j);
        % locking
        lnew = find(abs(Hj1) > tollck,1,'first') - 1;
        if lnew > 0
            [~,Ilock] = sort(select,'descend');
            if qzreal && ~isreal(lambda(Ilock(lnew))) && ...
                    imag(lambda(Ilock(lnew))) > 0
                % decrement lnew in case of missing complex conjugated value
                lnew = lnew - 1;
            end
            lambda(1:lnew) = lambda(Ilock(1:lnew));
            X(:,1:lnew) = X(:,Ilock(1:lnew));
            res(1:lnew) = res(Ilock(1:lnew));
            Hj1(1:lnew) = 0;
            Kj1(1:lnew) = 0;
        end
        H = zeros(m+1,m);
        K = zeros(m+1,m);
        H(1:j+1,1:j) = [F(1:j,1:j); Hj1];
        K(1:j+1,1:j) = [G(1:j,1:j); Kj1];
        
        %% new tensor U
        U1 = permute(reshape([reshape(permute(U1(:,1:m,:),[1,3,2]),[],m)*...
            Y(1:j,:)',reshape(U1(:,m+1,:),[],1)],r1,[],j+1),[1,3,2]);
        U2 = permute(reshape([reshape(permute(U2(:,1:m,:),[1,3,2]),[],m)*...
            Y(1:j,:)',reshape(U2(:,m+1,:),[],1)],r2,[],j+1),[1,3,2]);
        [UU1,US1,~] = svd(reshape(U1,r1,[]),'econ');
        [UU2,US2,~] = svd(reshape(U2,r2,[]),'econ');
        Us1 = diag(US1);
        Us2 = diag(US2);
        if isempty(tolrnk)
            rnew1 = sum(Us1 > max(r1,(j+1)*d1) * eps(max(Us1))); % see rank.m
            rnew2 = sum(Us2 > max(r2,(j+1)*d2) * eps(max(Us2))); % see rank.m
        else
            rnew1 = sum(Us1 > tolrnk);
            rnew2 = sum(Us2 > tolrnk);
        end
        U1 = reshape(UU1(:,1:rnew1)'*reshape(U1(1:r1,1:j+1,1:d1),r1,[]),...
            rnew1,[],d1);
        U2 = reshape(UU2(:,1:rnew2)'*reshape(U2(1:r2,1:j+1,1:d2),r2,[]),...
            rnew2,[],d2);
        
        %% new matrix Q
        Q1 = Q1*UU1(:,1:rnew1);
        Q2 = Q2*UU2(:,1:rnew2);
        
        if verbose > 0
            if verbose > 1, fprintf('\n'); end
            fprintf(' ==> restart %i: r1 = %i, r2 = %i, j = %i, l = %i\n',...
                nbrest,rnew1,rnew2,j,lnew);
            if verbose > 1, fprintf('\n'); end
        end
        
    end % implicitrestart


    function out = outputs(narg,flag,i)
        
        %% no convergence
        if flag
            [idx,in] = sortlam(lambda);
            if isempty(Sigma)
                lambda = lambda(idx(1:k));
                X = X(:,idx(1:k));
                res = res(idx(1:k));
            else
                lambda = lambda(in);
                X = X(:,in);
                res = res(in);
            end
            % number of converged eigenvalues
            nb = sum(res <= tolres);
            if isempty(Sigma)
                lambda = [lambda(res <= tolres);nan(k-nb,1)];
                X = [X(:,res <= tolres),nan(n,k-nb)];
                res = [res(res <= tolres);nan(k-nb,1)];
            end
            if nb == 0
                warning('CORK:NoEigsConverged',...
                    'None of the %i requested eigenvalues converged.',k);
            else
                warning('CORK:NotAllEigsConverged',...
                    'Only %i of the %i requested eigenvalues converged.',nb,k);
            end
        end
        
        %% set outputs
        if narg == 1
            out{1} = lambda;
            return
        end
        out{1} = X;
        if narg > 1
            out{2} = lambda;
        end
        if narg > 2
            out{3} = res;
        end
        if narg > 3
            out{4} = flag;
        end
        if narg > 4
            out{5} = struct('Lam',Lam(:,1:i),'Res',Res(:,1:i),...
                'J',J(1:i+1),'R1',R1(1:i+1),'R2',R2(1:i+1),'p',p,'m',m);
        end
        
    end % outputs


end % cork
