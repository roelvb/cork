% run_gun_lr

timings = 0;
verbose = 1;
usetrgt = 1;
solver = @cork_lr;

%% nlep formulation
NLEP = gun_init;
n = NLEP.n;

%% cork linearization
[L,d,Sigma] = gun_linearization(NLEP,100,1e-12);
d1 = NLEP.p + 1;
d2 = d - d1;
r = size(L.U,2);

%% rational Krylov shifts
Z = [2/3,(1+1i)/3,0,(-1+1i)/3,-2/3].';
gam = 300^2 - 200^2; mu = 250^2;
shifts = gam*Z + mu;
shift = shifts(3);

%% cork paramters
k = 20;
target = struct('Sigma',Sigma,'shift',shift);

%% cork options
m = 50;
p = 35;
rng(0); v0 = randn(n,1) + 1i*randn(n,1); v0 = v0/norm(v0);
funres = @(Lambda,X) gun_residual(Lambda,X,NLEP);
tolres = 1e-10;
opts = struct('m',m,'p',p,'v0',v0,'funres',funres,'tolres',tolres,...
    'shifts',shifts,'verbose',verbose);

%% solve nlep
if timings
    if usetrgt
        t0 = tic; [X,lambda,res] = solver(L,k,target,opts); toc(t0);
    else
        t0 = tic; [X,lambda,res] = solver(L,k,shift,opts); toc(t0);
    end
else
    if usetrgt
        t0 = tic; [X,lambda,res,flag,info] = solver(L,k,target,opts); toc(t0);
    else
        t0 = tic; [X,lambda,res,flag,info] = solver(L,k,shift,opts); toc(t0);
    end
    
    % plot
    subplot(3,1,1); hold off;
    [ResC,LamC,R] = plot_conv_eig(info.Res,info.Lam,info.J,tolres);
    ylim([1e-18,1e0]);
    title(['gun problem with low rank',' (d = ',num2str(d),')']);
    
    subplot(3,1,2); hold off;
    plot_dim_lr(info.J,info.R1,info.R2,m,d1,d2);
    
    subplot(3,1,3); hold off;
    [mem_rks,mem_cork] = plot_mem_lr(info.J,info.R1,info.R2,n,r,d1,d2);
end
