function varargout = cork(varargin)
% CORK  Find a few eigenvalues and eigenvectors of a linearization pencil
%   lambda = CORK(L) returns a vector of the 6 smalest magnitude eigenvalues of
%   the linearization pencil L, represented by the following structure:
%
%     L.A:     cell array of n x n matrices of length d
%     L.B:     cell array of n x n matrices of length d
%     L.M:     (d-1) x d matrix
%     L.N:     (d-1) x d matrix
%     L.Pfun:  function handle for P(lam) [optional]
%
%           [ A_0  A_1  ...  A_{d-1} ]         [ B_0  B_1  ...  B_{d-1} ]
%           [ ---------------------- ]         [ ---------------------- ]
%     L  =  [                        ] - lam * [                        ]
%           [     kron(M,eye(n))     ]         [     kron(N,eye(n))     ]
%           [                        ]         [                        ]
%
%   [X,lambda,res] = CORK(L) returns a vector lambda of the 6 smalest magnitude
%   eigenvalues,a matrix X whose columns are the corresponding eigenvectors, and
%   a vector res containing the residuals.
%
%   [X,lambda,res,flag] = CORK(L) also returns a convergence flag. If flag is 0
%   then all the eigenvalues converged; otherwise not all converged.
%
%   CORK(L,k) returns the k largest magnitude eigenvalues.
%
%   CORK(L,k,sigma) returns the k eigenvalues closest to the real or complex
%   scalar sigma.
%
%   CORK(L,k,target) returns the eigenvalues inside the target set target.Sigma
%   and closest to the shift target.sigma:
%     target.Sigma:  vector containing the points of a polygonal target set in
%                    the complex plane
%     target.shift:  scalar shift
%   k is a guess of the number of eigenvalues inside the target set.
%
%   CORK(L,k,sigma,opts) sets the algorithm's parameters to the values in the
%   structure opts:
%     opts.m:        maximum number of ritz values
%                    [ positive integer {max(3*k,20)} ]
%     opts.p:        number of restarted ritz values
%                    [ positive integer {max(2*k,10)} ]
%     opts.minit:    min number of iterations
%                    [ positive integer {m} ]
%     opts.maxrest:  maximum number or restarts
%                    [ positive integer {50} ]
%     opts.v0:       starting vector
%                    [ vector {randomly generated} ]
%     opts.funres:   function handle for residual (Lambda[vector], X[matrix])
%                    [ @(Lambda,X) {estimated residual norm} ]
%     opts.tolres:   tolerance for residual
%                    [ positive scalar {100*eps} ]
%     opts.tolrnk:   tolerance for truncating rank
%                    [ positive scalar {[]} ]
%     opts.tollck:   tolerance for locking
%                    [ positive scalar {eps} ]
%     opts.shifts:   cyclically repeated shifts for the rational Krylov process
%                    [ vector {sigma} ]
%     opts.reuselu:  reuse of LU-factorizations
%                    [ {true} | false ]
%     opts.verbose:  level of display
%                    [ {0} | 1 | 2 ]
%
%   [X,lambda,res,flag,info] = CORK(L) also returns a structure info:
%     info.Lam:  matrix with Ritz values in each iteration 
%     info.Res:  matrix with residuals in each iteraion
%     info.J:    vector with dimension of Krylov subspace in each iteration
%     info.R:    vector with rank of subspace Q in each iteration
%
%   See also CORK_LR, EIGS.

%   Reference:
%   R. Van Beeumen, K. Meerbergen, and W. Michiels. Compact rational Krylov
%   methods for nonlinear eigenvalue problems. SIAM J. Matrix Anal. Appl. 36(2),
%   pp. 820-838, 2015.
%
%   author:  Roel Van Beeumen
%   version: October 3, 2018

%% check inputs
[L,n,d,k,m,p,minit,maxrest,sigma,Sigma,v0,funres,tolres,tolrnk,tollck,...
    shifts,reuselu,maxreorth,resfreq,verbose,info] = ...
    checkInputs(nargout,varargin{:});

%% initialization
[Q,U,r,j,H,K,X,lambda,res,Lam,Res,J,R,LU,SHIFTS,NNZ] = initialize(v0);

%% CORK loop
i = 1; nbrest = 0; l = 0; count = 0; flag = true;
while i <= m + maxrest*(m-p)
    
    % output
    out = nargout > 4 || (i >= minit && mod(i,resfreq) == 0) ...
                      || (i == m + maxrest*(m-p));
    
    % CORK step
    r = corkstep(shifts(i),r,j,i);
    
    % compute Ritz pairs and residuals
    if out, flag = ritzpairs(r,j,i,l); elseif verbose > 1, fprintf('\n'); end
    
    % check for convergence
    if ~flag, break, end
    
    % restart
    if j == m
        if maxrest > 0 && nbrest < maxrest
            nbrest = nbrest + 1;
            [r,j,l] = implicitrestart(r,nbrest);
        else
            break
        end
    end
    
    % increment i & j
    i = i + 1;
    j = j + 1;
    
end

%% outputs
varargout = outputs(nargout,flag,i);


% ---------------------------------------------------------------------------- %
% Nested functions
% ---------------------------------------------------------------------------- %

% checkInputs: error checks the inputs to CORK and also derives some variables
% '''''''''''' from them:
%
%   L          linearization pencil
%   n          block size of L
%   d          number of block rows of L
%   k          number of eigenvalues to be computed [ 6 ]
%   m          maximum number of ritz values [ 30 ]
%   p          number of restarted ritz values [ 20 ]
%   minit      minimum number of iterations [ m ]
%   maxrest    maximum number of restarts [ 50 ]
%   sigma      scalar for finding the eigenvalues closest to [ 0 ]
%   Sigma      vector containing the points of a polygonal target set [ ]
%   v0         starting vector
%   funres     function handle for residual R(Lambda,X)
%   tolres     tolerance for residual
%   tolrnk     tolerance for truncating rank
%   tollck     tolerance for locking
%   shifts     cyclically repeated shifts for the rational Krylov process
%   reuselu    positive integer for reuse of LU-factorizations of A(sigma)
%   maxreorth  maximum number of reorthogonalizations [ 2 ]
%   resfreq    frequency for computing residuals [ 5 ]
%   verbose    level of display [ {0} | 1 | 2 ]
%   info       [ true | {false} ]
    function [L,n,d,k,m,p,minit,maxrest,sigma,Sigma,v0,funres,tolres,...
            tolrnk,tollck,shifts,reuselu,maxreorth,resfreq,verbose,info] = ...
            checkInputs(narg,varargin)
        
        %% linearization pencil
        L = varargin{1};
        n = size(L.A{1},1);
        d = length(L.A);
        
        %% number of eigenvalues
        if length(varargin) > 1
            k = varargin{2};
        else
            k = 6;
        end
        
        %% eigenvalues in Sigma and closest to sigma
        Sigma = [];
        if length(varargin) > 2
            if ~isstruct(varargin{3})
                sigma = varargin{3};
            else
                sigma = varargin{3}.shift;
                Sigma = varargin{3}.Sigma;
            end
        else
            sigma = 0;
        end
        
        %% set defaults
        m = max(3*k,20);
        p = max(2*k,10);
        minit = m;
        maxrest = 50;
        funres = [];
        tolres = 100*eps;
        tolrnk = [];
        tollck = eps;
        v0 = randn(n,1);
        shifts = sigma;
        reuselu = true;
        maxreorth = 2;
        if narg > 4, resfreq = 1; else, resfreq = 5; end
        verbose = 0;
        info = narg > 4;
        
        %% process the input opts
        if length(varargin) > 3
            opts = varargin{4};
            if ~isa(opts,'struct')
                error('The input argument ''opts'' must be a struct.');
            end
            if isfield(opts,'m') && ~isempty(opts.m)
                m = opts.m;
                minit = m;
            end
            if isfield(opts,'p') && ~isempty(opts.p)
                p = opts.p;
            end
            if isfield(opts,'minit') && ~isempty(opts.minit)
                minit = opts.minit;
            end
            if isfield(opts,'maxrest') && ~isempty(opts.maxrest)
                maxrest = opts.maxrest;
            end
            if isfield(opts,'v0') && ~isempty(opts.v0)
                v0 = opts.v0;
            end
            if isfield(opts,'funres') && ~isempty(opts.funres)
                funres = opts.funres;
            end
            if isfield(opts,'tolres') && ~isempty(opts.tolres)
                tolres = opts.tolres;
            end
            if isfield(opts,'tolrnk') && ~isempty(opts.tolrnk)
                tolrnk = opts.tolrnk;
            end
            if isfield(opts,'tollck') && ~isempty(opts.tollck)
                tollck = opts.tollck;
            end
            if isfield(opts,'shifts') && ~isempty(opts.shifts)
                shifts = opts.shifts;
            end
            if isfield(opts,'reuselu') && ~isempty(opts.reuselu)
                reuselu = opts.reuselu;
            end
            if isfield(opts,'verbose') && ~isempty(opts.verbose)
                verbose = opts.verbose;
            end
        end
        if isscalar(shifts)
            reuselu = true;
        end
        nb = ceil( (m + maxrest*(m-p) + 1) / floor(length(shifts)) );
        shifts = repmat(shifts(:),nb,1);
        
    end % checkInputs


    function [Q,U,r,j,H,K,X,lambda,res,Lam,Res,J,R,LU,SHIFTS,NNZ] ...
            = initialize(v0)
        
        %% matrix Q
        [Q,U0] = qr(reshape(v0/norm(v0),n,[]),0);
        r = size(Q,2);
        
        %% tensor U
        U = zeros(r,m+1,d);
        U(1:r,1,1:size(U0,1)) = U0;
        j = 1;
        
        %% Hessenberg matrices H and K
        H = zeros(m+1,m);
        K = zeros(m+1,m);
        
        %% eigenpairs and residuals
        X = nan(n,m);
        lambda = nan(m,1);
        res = nan(m,1);
        
        %% info variables
        if info
            Lam = nan(m,m + maxrest*(m-p));
            Res = nan(m,m + maxrest*(m-p));
        else
            Lam = [];
            Res = [];
        end
        J = [1;zeros(m + maxrest*(m-p),1)];
        R = [r;zeros(m + maxrest*(m-p),1)];
        
        %% lu variables
        LU = cell(1); SHIFTS = [];
        NNZ = struct('A',cellfun(@(x) nnz(x) > 0,L.A),...
            'B',cellfun(@(x) nnz(x) > 0,L.B));
        
    end % initialize


    function rnew = corkstep(shift,r,j,i)
        
        if (verbose > 0 && out) || verbose > 1, fprintf('iteration %i:',i); end
        eta = 1/sqrt(2); % reorthogonalization constant
        
        %% compute next vector
        if i == 1 || shift == K(j,j-1)/H(j,j-1)
            t = flipud(eye(j,1));
            v = Q(:,1:r)*reshape(U(1:r,j,1:d),r,[]);
        else
            [Qj,~] = qr(K(1:j,1:j-1) - shift*H(1:j,1:j-1));
            t = Qj(:,end);
            idx = abs(t) > eps;
            v = Q(:,1:r)*(reshape(U(1:r,idx,1:d),r,[])*kron(speye(d),t(idx)));
        end
        if verbose > 1, tstart = tic; end
        [v1,MsN1inv0,MsN1invN] = backslash(shift,v);
        if verbose > 1, telapsed = toc(tstart);
            fprintf('\n    system solve: %f seconds',telapsed);
        end
        
        %% level 1 orthogonalization
        q = v1;
        delta = inf; nborth = 0;
        while norm(q) < eta*delta && nborth <= maxreorth
            delta = norm(q);
            if nborth == 0
                u1 = Q(:,1:r)'*v1;
                q = q - Q(:,1:r)*u1;
            else
                q = q - Q(:,1:r)*(Q(:,1:r)'*q);
            end
            nborth = nborth + 1;
            if verbose > 1 && nborth > 1
                fprintf('\n    reorthogonalization Q');
            end
        end
        delta = norm(q);
        % update Q
        if delta > eps
            rnew = r + 1;
            q = q/delta;
            Q(:,rnew) = q;
        else
            rnew = r;
        end
        
        %% level 2 orthogonalization
        if rnew > r
            U(rnew,:,:) = 0;
            u1 = [u1;q'*v1];
        end
        if i == 1 || shift == K(j,j-1)/H(j,j-1)
            u2 = reshape(U(1:rnew,j,1:d),rnew,[]);
        else
            u2 = reshape(U(1:rnew,idx,1:d),rnew,[])*kron(speye(d),t(idx));
        end
        u2 = u2*MsN1invN.' - bsxfun(@times,MsN1inv0.',u1);
        u = [u1;reshape(u2,[],1)];
        delta = inf; nborth = 0;
        while norm(u) < eta*delta && nborth <= maxreorth
            delta = norm(u);
            h = reshape(conj(permute(U(1:rnew,1:j,1:d),[2,1,3])),[],d*rnew)*u;
            u = u - reshape(permute(U(1:rnew,1:j,1:d),[1,3,2]),[],j)*h;
            H(1:j,j) = H(1:j,j) + h;
            nborth = nborth + 1;
            if verbose > 1 && nborth > 1
                fprintf('\n    reorthogonalization U');
            end
        end
        K(1:j,j) = shift*H(1:j,j) + t;
        H(j+1,j) = norm(u);
        K(j+1,j) = shift*H(j+1,j);
        % update U
        u = u/H(j+1,j);
        U(1:rnew,j+1,1:d) = reshape(u,[],d);
        
        %% dimensions
        J(i+1) = j+1;
        R(i+1) = rnew;
        
    end % corkstep


    function [x1,MsN1inv0,MsN1invN] = backslash(shift,y)
        
        %% check for new shift
        if ~reuselu
            newlu = true;
        else
            newlu = false;
            if isempty(SHIFTS)
                idx = 1;
                SHIFTS(idx) = shift;
                newlu = true;
            else
                idx = find(SHIFTS == shift,1,'first');
                if isempty(idx)
                    idx = length(SHIFTS) + 1;
                    SHIFTS(idx) = shift;
                    newlu = true;
                end
            end
        end
        
        %% initialize
        m0 = L.M(1:d-1,1);
        M1 = L.M(1:d-1,2:d);
        n0 = L.N(1:d-1,1);
        N1 = L.N(1:d-1,2:d);
        
        %% compute kronecker coefficients
        MsN1 = M1 - shift*N1;
        msn0 = m0 - shift*n0;
        MsN1inv0 = MsN1\msn0;
        MsN1invN = MsN1\L.N(1:d-1,1:d);
        
        %% build At
        if newlu
            if isfield(L,'Pfun')
                At = L.Pfun(shift);
            else
                At = L.A{1} - shift*L.B{1};
                for ii = 2:d
                    At = At - MsN1inv0(ii-1)*(L.A{ii} - shift*L.B{ii});
                end
            end
        end
        
        %% intermediate x used in yt
        x = y*MsN1invN.';
        
        %% build yt
        yt = L.B{1}*y(:,1);
        for ii = 2:d
            if NNZ.A(ii) && NNZ.B(ii)
                yt = yt - L.A{ii}*x(:,ii-1) + L.B{ii}*(shift*x(:,ii-1)+y(:,ii));
            elseif NNZ.A(ii)
                yt = yt - L.A{ii}*x(:,ii-1);
            elseif NNZ.B(ii)
                yt = yt + L.B{ii}*(shift*x(:,ii-1)+y(:,ii));
            end
        end
        
        %% compute x1 = At\yt
        if ~reuselu
            x1 = At\yt;
        else
            if newlu
                if issparse(At)
                    [AL,AU,AP,AQ,AR] = lu(At);
                    x1 = AQ * (AU \ (AL \ (AP * (AR \ yt))));
                    % improve accuracy
                    resid = yt - At*x1;
                    err = AQ * (AU \ (AL \ (AP * (AR \ resid))));
                    x1 = x1 + err;
                    LU{idx} = struct('A',At,'L',AL,'U',AU,'P',AP,'Q',AQ,'R',AR);
                else
                    [AL,AU,Ap] = lu(At,'vector');
                    x1 = AU \ (AL \ yt(Ap,:));
                    % improve accuracy
                    resid = yt - At*x1;
                    err = AU \ (AL \ resid(Ap,:));
                    x1 = x1 + err;
                    LU{idx} = struct('A',At,'L',AL,'U',AU,'p',Ap);
                end
            else
                if isfield(LU{idx},'R')
                    x1 = LU{idx}.Q * (LU{idx}.U \ (LU{idx}.L \ ...
                        (LU{idx}.P * (LU{idx}.R \ yt))));
                    % impove accuracy
                    resid = yt - LU{idx}.A*x1;
                    err = LU{idx}.Q * (LU{idx}.U \ (LU{idx}.L \ ...
                        (LU{idx}.P * (LU{idx}.R \ resid))));
                    x1 = x1 + err;
                else
                    x1 = LU{idx}.U \ (LU{idx}.L \ yt(LU{idx}.p,:));
                    % improve accuracy
                    resid = yt - LU{idx}.A*x1;
                    err = LU{idx}.U \ (LU{idx}.L \ resid(LU{idx}.p,:));
                    x1 = x1 + err;
                end
            end
        end
        
    end % backslash


    function flag = ritzpairs(r,j,i,l)
        
        %% QZ factorization
        qzreal = isreal(sigma) && isreal(K) && isreal(H);
        if qzreal
            [G,F,~,~,S,~] = qz(K(1:j,1:j),H(1:j,1:j),'real');
        else
            [G,F,~,~,S,~] = qz(K(1:j,1:j),H(1:j,1:j));
        end
        
        %% ritz pairs
        ordlam = ordeig(G,F);
        lambda(l+1:j) = ordlam(l+1:j);
        [idx,in] = sortlam(lambda(1:j));
        if qzreal
            ii = l+1;
            while ii <= j
                if isreal(lambda(ii))
                    S(:,ii) = S(:,ii)/norm(S(:,ii));
                    ii = ii + 1;
                else
                    Sr = S(:,ii);
                    Si = S(:,ii+1);
                    S(:,ii) = (Sr + 1i*Si)/norm(Sr + 1i*Si);
                    S(:,ii+1) = (Sr - 1i*Si)/norm(Sr - 1i*Si);
                    ii = ii + 2;
                end
            end
        else
            for ii = l+1:j
                S(:,ii) = S(:,ii)/norm(S(:,ii));
            end
        end
        X(:,l+1:j) = Q(:,1:r)*(U(1:r,1:j+1,1)*(H(1:j+1,1:j)*S(:,l+1:j)));
        for ii = l+1:j
            X(:,ii) = X(:,ii)/norm(X(:,ii));
        end
        
        %% residuals
        if isempty(funres)
            res(l+1:j) = abs((K(j+1,j) - lambda(l+1:j)*H(j+1,j)).*S(j,l+1:j).');
        else
            res(l+1:end) = nan;
            in2 = in; in2(1:l) = false;
            res(in2) = funres(lambda(in2),X(:,in2));
        end
        
        %% info
        if info
            Lam(1:j,i) = lambda(1:j);
            Res(1:j,i) = res(1:j);
        end
        
        %% check for convergence
        if j < k
            count = 0;
        elseif isempty(Sigma)
            if all(res(idx(1:k)) <= tolres)
                count = count + 1;
            else
                count = 0;
            end
        else
            if all(res(in) <= tolres)
                count = count + 1;
            else
                count = 0;
            end
        end
        flag = true;
        if i >= minit
            if (resfreq == 1 && count >= 5) || (resfreq > 1 && count > 1)
                flag = false;
            end
        end
        
        if verbose > 0 && out
            if verbose > 1, fprintf('\n    eigenvalues:'); end
            if isempty(Sigma)
                fprintf(' %i < %.2e\n',sum(res(idx(1:min(j,k))) <= tolres),...
                    tolres);
            else
               fprintf(' %i (of %i) < %.2e\n',...
                   sum(res(in) <= tolres),sum(in),tolres);
            end
            if ~flag
                if verbose > 1, fprintf('\n'); end
                if isempty(Sigma)
                    fprintf(' ==> %i converged Ritz pairs\n',k);
                else
                    fprintf([' ==> all Ritz pairs inside target set ',...
                        'converged\n']);
                end
                if verbose > 1, fprintf('\n'); end
            end
        end
        
        %% convergence
        if ~flag
            if isempty(Sigma)
                lambda = lambda(idx(1:k));
                X = X(:,idx(1:k));
                res = res(idx(1:k));
            else
                lambda = lambda(in);
                X = X(:,in);
                res = res(in);
            end
        end
        
    end % ritzpairs


    function [idx,in] = sortlam(lambda)
        
        if isempty(Sigma)
            [~,idx] = sort(abs(lambda - sigma));
            in = true(length(lambda),1);
        else
            in = inpolygon(real(lambda),imag(lambda),real(Sigma),imag(Sigma));
            Ilambda = (1:length(lambda)).';
            Ilamin = Ilambda(in);
            Ilamout = Ilambda(~in);
            [~,Iin] = sort(abs(lambda(in) - sigma));
            [~,Iout] = sort(abs(lambda(~in) - sigma));
            idx = [Ilamin(Iin);Ilamout(Iout)];
        end
        
    end % sortlam


    function [rnew,j,lnew] = implicitrestart(r,nbrest)
        
        %% QZ factorization
        qzreal = isreal(sigma) && isreal(K) && isreal(H);
        if qzreal
            [G,F,Y,Z] = qz(K(1:m,:),H(1:m,:),'real');
        else
            [G,F,Y,Z] = qz(K(1:m,:),H(1:m,:));
        end
        
        %% select ritz values and reorder QZ factorization
        Ilambda = sortlam(lambda);
        if qzreal && ~isreal(lambda(Ilambda(p))) && ...
                imag(lambda(Ilambda(p))) == -imag(lambda(Ilambda(p+1)))
            % increment p in case of complex conjugated ritz value
            j = p + 1;
        else
            j = p;
        end
        selres = zeros(m,1);
        [~,Ires] = sort(res);
        selres(Ires) = m:-1:1;
        selres(res == 0) = 2*m;
        select = zeros(m,1);
        if qzreal
            ii = 0;
            while ii < j
                if isreal(lambda(Ilambda(ii+1)))
                    select(Ilambda(ii+1)) = selres(Ilambda(ii+1));
                    ii = ii + 1;
                else
                    select(Ilambda(ii+1:ii+2)) = selres(Ilambda(ii+1));
                    ii = ii + 2;
                end
            end
        else
            select(Ilambda(1:j)) = selres(Ilambda(1:j));
        end
        [G,F,Y,Z] = ordqz(G,F,Y,Z,select);
        
        %% new Hessenberg matrices H and K
        Hj1 = H(m+1,:)*Z(:,1:j);
        Kj1 = K(m+1,:)*Z(:,1:j);
        % locking
        lnew = find(abs(Hj1) > tollck,1,'first') - 1;
        if lnew > 0
            [~,Ilock] = sort(select,'descend');
            if qzreal && ~isreal(lambda(Ilock(lnew))) && ...
                    imag(lambda(Ilock(lnew))) > 0
                % decrement lnew in case of missing complex conjugated value
                lnew = lnew - 1;
            end
            lambda(1:lnew) = lambda(Ilock(1:lnew));
            X(:,1:lnew) = X(:,Ilock(1:lnew));
            res(1:lnew) = res(Ilock(1:lnew));
            Hj1(1:lnew) = 0;
            Kj1(1:lnew) = 0;
        end
        H = zeros(m+1,m);
        K = zeros(m+1,m);
        H(1:j+1,1:j) = [F(1:j,1:j); Hj1];
        K(1:j+1,1:j) = [G(1:j,1:j); Kj1];
        
        %% new tensor U
        U = permute(reshape([reshape(permute(U(:,1:m,:),[1,3,2]),[],m)*...
            Y(1:j,:)',reshape(U(:,m+1,:),[],1)],r,[],j+1),[1,3,2]);
        [UU,US,~] = svd(reshape(U,r,[]),'econ');
        Us = diag(US);
        if isempty(tolrnk)
            rnew = sum(Us > max(r,(j+1)*d) * eps(max(Us))); % see rank.m
        else
            rnew = sum(Us > tolrnk);
        end
        U = reshape(UU(:,1:rnew)'*reshape(U(1:r,1:j+1,1:d),r,[]),rnew,[],d);
        
        %% new matrix Q
        Q = Q*UU(:,1:rnew);
        
        if verbose > 0
            if verbose > 1, fprintf('\n'); end
            fprintf(' ==> restart %i: r = %i, j = %i, l = %i\n',...
                nbrest,rnew,j,lnew);
            if verbose > 1, fprintf('\n'); end
        end
        
    end % implicitrestart


    function out = outputs(narg,flag,i)
        
        %% no convergence
        if flag
            [idx,in] = sortlam(lambda);
            if isempty(Sigma)
                lambda = lambda(idx(1:k));
                X = X(:,idx(1:k));
                res = res(idx(1:k));
            else
                lambda = lambda(in);
                X = X(:,in);
                res = res(in);
            end
            % number of converged eigenvalues
            nb = sum(res <= tolres);
            if isempty(Sigma)
                lambda = [lambda(res <= tolres);nan(k-nb,1)];
                X = [X(:,res <= tolres),nan(n,k-nb)];
                res = [res(res <= tolres);nan(k-nb,1)];
            end
            if nb == 0
                warning('CORK:NoEigsConverged',...
                    'None of the %i requested eigenvalues converged.',k);
            else
                warning('CORK:NotAllEigsConverged',...
                    'Only %i of the %i requested eigenvalues converged.',nb,k);
            end
        end
        
        %% set outputs
        if narg == 1
            out{1} = lambda;
            return
        end
        out{1} = X;
        if narg > 1
            out{2} = lambda;
        end
        if narg > 2
            out{3} = res;
        end
        if narg > 3
            out{4} = flag;
        end
        if narg > 4
            out{5} = struct('Lam',Lam(:,1:i),'Res',Res(:,1:i),...
                'J',J(1:i+1),'R',R(1:i+1),'p',p,'m',m);
        end
        
    end % outputs


end % cork
