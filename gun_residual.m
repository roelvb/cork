function R = gun_residual(Lambda,X,NLEP)

%% initialization
k = length(Lambda);
R = zeros(k,1);
if k <= 0, return, end

%% Denominator
Den = zeros(size(Lambda));
for i = 0:NLEP.p
    Den = Den + abs(Lambda.^i)*NLEP.nrmB(i+1);
end
for i = 1:NLEP.q
    Den = Den + NLEP.fa{i}(Lambda)*NLEP.nrmC(i);
end

%% A(lambda)*x
AX = zeros(NLEP.n,k);
for i = 0:NLEP.p
    AX = AX + NLEP.B{i+1}*X*spdiags(Lambda.^i,0,k,k);
end
for i = 1:NLEP.q
    AX = AX + NLEP.C{i}*X*spdiags(NLEP.f{i}(Lambda),0,k,k);
end

%% residual
for i = 1:k
    R(i) = norm(AX(:,i))/Den(i);
end

end
