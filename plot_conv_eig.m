function [ResC,LamC,R] = plot_conv_eig(Res,Lam,J,tol,plotopts)

if nargin < 5, plotopts = '-k'; end
if nargin < 4, tol = []; end

[n,k] = size(Lam);

% restarts
R = 0; nb = 1;
for j = 1:length(J)-1
    if J(j) > J(j+1)
        nb = nb + 1;
        R(nb) = j-1;
    end
end

% sort residuals
ResC = zeros(n,k);
LamC = zeros(n,k);
for j = 1:nb
    b = R(j) + 1;
    if j < nb
        e = R(j+1);
    else
        e = k;
    end
    [ResT,LamT] = sort_res(Res(:,b:e),Lam(:,b:e),J(R(j)+2) - 2);
    ResC(:,b:e) = ResT;
    LamC(:,b:e) = LamT;
end

% plot convergence
for j = 1:nb
    b = R(j) + 1;
    if j < nb
        e = R(j+1);
    else
        e = k;
    end
    for i = 1:n
        semilogy(b:e,ResC(i,b:e),plotopts); hold on;
    end
end

% plot tolerance
if ~isempty(tol)
    semilogy([0,k],[tol,tol],':k');
end

% plot restarts
Rmin = 10^floor(log10(min(min(Res(Res ~= 0)))));
Rmax = 10^ceil(log10(max(max(Res))));
for j = 1:nb
    if R(j) > 0
        plot([R(j),R(j)],[Rmin,Rmax],'--b');
    end
end

% plot labels
xlabel('iteration');
ylabel('residual');

end


function [ResC,LamC] = sort_res(Res,Lam,p)

[n,k] = size(Res);
ResC = zeros(n,k);
LamC = zeros(n,k);

% sort residuals
ResT = zeros(n,k);
LamT = zeros(n,k);
for i = 1:k
    [~,idx] = sort(Res(1:p+i,i));
    ResT(1:p+i,i) = Res(idx,i);
    LamT(1:p+i,i) = Lam(idx,i);
end

% first iteration
ResC(1:p+1,1) = ResT(1:p+1,1);
LamC(1:p+1,1) = LamT(1:p+1,1);

% next iterations
for i = 2:k
    sRes = ResT(1:p+i,i);
    sLam = LamT(1:p+i,i);
    for j = 1:p+i-1
        [~,ii] = min(abs(sLam-LamC(j,i-1)));
        ResC(j,i) = sRes(ii);
        LamC(j,i) = sLam(ii);
        sRes(ii) = [];
        sLam(ii) = [];
    end
    ResC(p+i,i) = sRes;
    LamC(p+i,i) = sLam;
end

end
