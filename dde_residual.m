function R = dde_residual(Lambda,X,A0,A1,tau)

% constants
k = length(Lambda);
nA0 = 1.013617361686411e+07;  % norm(A0,1)
nA1 = 2;                      % norm(A1,1)

% Denominator
Den = nA0 + abs(Lambda) + abs(exp(-tau*Lambda))*nA1;

R = zeros(k,1);
for i = 1:k
    lam = Lambda(i);
    x = X(:,i);
    R(i) = norm(A0*x - lam*x + exp(-tau*lam)*(A1*x))/Den(i);
end
