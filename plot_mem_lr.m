function [mem_rks,mem_cork] = plot_mem_lr(J,R1,R2,n1,n2,d1,d2,plotfun)

if nargin < 8
    plotfun = @plot;
end

nb = length(J);

% RKS
M_rks = zeros(nb,1);
for i = 1:nb
    M_rks(i) = (n1*d1 + n2*d2)*J(i);
end

% CORK
M_cork = zeros(nb,1);
for i = 1:nb
    M_cork(i) = (n1*R1(i) + R1(i)*J(i)*d1) + (n2*R2(i) + R2(i)*J(i)*d2);
end

c = 8/1e6;
mem_rks = c*M_rks;
mem_cork = c*M_cork;

plotfun((0:nb-1)',mem_rks,'o:k'); hold on;
plotfun((0:nb-1)',mem_cork,'+:b');
xlabel('iteration');
ylabel('memory (MB)');
legend('RKS','CORK','Location','NorthWest');

end
