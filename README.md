CORK Matlab toolbox
===================

- Author:  Roel Van Beeumen
- Date:    October 2, 2018
- Version: 0.3


Main files
----------

- cork.m:    find a few eigenvalues and eigenvectors of a linearization pencil.
- cork_lr.m: find a few eigenvalues and eigenvectors of a low rank linearization pencil.

Instructions can be found in the header of the files.


Examples
--------

The examples contains the numerical experiments for a delay eigenvalue problem and the 'gun' problem. For running the latter one, the user should first install the [NLEVP collection](http://www.mims.manchester.ac.uk/research/numerical-analysis/nlevp.html).

- run_dde_norest.m
- run_dde_rest.m
- run_gun.m
- run_gun_lr.m
- run_gun_nleigs.m


Auxiliary files
---------------

- plot_conv_eig.m
- plot_dim.m
- plot_dim_lr.m
- plot_mem.m
- plot_mem_lr.m

- dde5000.mat
- dde_residual.m
- gun_init.m
- gun_linearization.m
- gun_residual.m
- newtonapprox.m

Instructions can be found in the header of the files.


Paper
-----

R. Van Beeumen, K. Meerbergen, and W. Michiels
_Compact rational Krylov methods for nonlinear eigenvalue problems_
SIAM Journal on Matrix Analysis and Applications, 36 (2), 820-838, 2015.

All scientific publications, for which the software has been used, must mention the use of the software, and must refer to the above publication.
