function NLEP = gun_init(islowrank)

%% default
if nargin < 1, islowrank = true; end

%% gun problem
coeffs = nlevp('gun');
n = size(coeffs{1},1);
sigma1 = 0;
sigma2 = 108.8774;

%% polynomial matrices
B{1} = coeffs{1};  % K
B{2} = -coeffs{2}; % -M
nrmB(1) = norm(B{1},1);
nrmB(2) = norm(B{2},1);
p = length(B) - 1;

%% nonlinear matrices
C{1} = coeffs{3}; % W1
C{2} = coeffs{4}; % W2
nrmC(1) = norm(C{1},1);
nrmC(2) = norm(C{2},1);
[L{1},U{1}] = compactlu(C{1});
[L{2},U{2}] = compactlu(C{2});
q = length(C);

%% nonlinear functions
f{1} = @(x) 1i*sqrt(x - sigma1^2);
f{2} = @(x) 1i*sqrt(x - sigma2^2);

%% nonlinear abs functions
fa{1} = @(x) sqrt(abs(x - sigma1^2));
fa{2} = @(x) sqrt(abs(x - sigma2^2));

%% nonlinear matrix functions
fm{1} = @(x) 1i*sqrtm(x - sigma1^2*x^0);
fm{2} = @(x) 1i*sqrtm(x - sigma2^2*x^0);

%% nonlinear matrix-valued function
Afun = @(lam) B{1} + lam*B{2} + f{1}(lam)*C{1} + f{2}(lam)*C{2};

%% nlep
if ~islowrank
    NLEP = struct('n',n,'p',p,'q',q,'B',{B},'C',{C},...
        'f',{f},'fa',{fa},'fm',{fm},'Afun',Afun,...
        'sigma1',sigma1,'sigma2',sigma2,'nrmB',nrmB,'nrmC',nrmC);
else
    NLEP = struct('n',n,'p',p,'q',q,'B',{B},'C',{C},'L',{L},'U',{U},...
        'f',{f},'fa',{fa},'fm',{fm},'Afun',Afun,...
        'sigma1',sigma1,'sigma2',sigma2,'nrmB',nrmB,'nrmC',nrmC);
end

end % gun_init


function [L,U] = compactlu(W)

%% nonzero rows and columns
[i,j] = find(W);
idx = unique(union(i,j));
Wc = W(idx,idx);

%% compact lu
n = size(W,1); k = size(Wc,1);
[Lc,Uc] = lu(Wc);
L = spalloc(n,k,nnz(Lc)); L(idx,:) = Lc;
U = spalloc(n,k,nnz(Uc)); U(idx,:) = Uc';

end % compactlu
