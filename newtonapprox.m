function [L,d,conv,sigma,xi,beta,gdd,nrmD,D] = ...
    newtonapprox(NLEP,Sigma,Xi,maxdgr,tollin)

%% default inputs
if nargin < 3, Xi = inf; end
if nargin < 4, maxdgr = 100; end
if nargin < 5, tollin = 1e-12; end

%% parameters from inputs
n = NLEP.n;
p = NLEP.p;
q = NLEP.q;
isfunm = isfield(NLEP,'fm');
islowrank = isfield(NLEP,'L') && isfield(NLEP,'U');

%% lega-bagby points
[sigma,xi,beta] = lejabagby(Sigma,Xi,Sigma,maxdgr+1,false,p);

%% Compute scalar generalized divided differences of polynomials part
sgddp = zeros(p+1,length(sigma));
for ii = 1:p+1
    if isfunm
        sgddp(ii,:) = cell2mat(ratnewtoncoeffsm(@(x) x^(ii-1),sigma,xi,beta));
    else
        sgddp(ii,:) = cell2mat(ratnewtoncoeffs(@(x) x^(ii-1),sigma,xi,beta));
    end
end

%% Compute scalar generalized divided differences of nonlinear part
sgddq = zeros(q,length(sigma));
for ii = 1:q
    if isfunm
        sgddq(ii,:) = cell2mat(ratnewtoncoeffsm(NLEP.fm{ii},sigma,xi,beta));
    else
        sgddq(ii,:) = cell2mat(ratnewtoncoeffs(NLEP.f{ii},sigma,xi,beta));
    end
end

%% truncate approximation
gdd = [sgddp;sgddq];
nrmD = max(abs(gdd),[],1);
d = maxdgr; conv = false;
while nrmD(d) < tollin
    conv = true;
    d = d - 1;
end
if conv, d = d - 1; else, warning('Linearization not converged!'); end

%% compute rational newton coefficients
D = cell(1,d+1);
for i = 0:d
    if ~islowrank || i <= p
        D{i+1} = spalloc(n,n,n);
        for j = 1:p+1
            D{i+1} = D{i+1} + sgddp(j,i+1)*NLEP.B{j};
        end
        for j = 1:q
            D{i+1} = D{i+1} + sgddq(j,i+1)*NLEP.C{j};
        end
    else
        Lt = cell(1,q);
        for j = 1:q
            Lt{j} = sgddq(j,i+1)*NLEP.L{j};
        end
        D{i+1} = cell2mat(Lt);
    end
end

%% linearization
A = cell(1,d+1);
B = cell(1,d+1);

% linear relations basis functions
M = [diag(sigma(1:d)), zeros(d,1)] + [zeros(d,1), diag(beta(2:d+1))];
N = eye(d,d+1) + [zeros(d,1), diag(beta(2:d+1)./xi(1:d))];

% coefficient matrices
if ~islowrank
    for i = 1:d+1
        A{i} = D{i};
        B{i} = spalloc(n,n,0);
    end
    % output
    if ~isfield(NLEP,'Afun')
        L = struct('A',{A},'B',{B},'M',M,'N',N);
    else
        L = struct('A',{A},'B',{B},'M',M,'N',N,...
            'Pfun',@(lam) NLEP.Afun(lam));
    end
else
    U = cell2mat(NLEP.U);
    % full rank part
    for i = 1:p-1
        A{i} = D{i};
        B{i} = spalloc(n,n,0);
    end
    A{p} = D{p} - sigma(p)/beta(p+1)*D{p+1};
    B{p} = -1/beta(p+1)*D{p+1};
    % low rank part
    r = size(U,2);
    A{p+1} = spalloc(n,r,0);
    B{p+1} = spalloc(n,r,0);
    for i = p+2:d+1
        A{i} = D{i};
        B{i} = spalloc(n,r,0);
    end
    % output
    if ~isfield(NLEP,'Afun')
        L = struct('A',{A},'B',{B},'M',M,'N',N,'U',U,'p',p-1);
    else
        L = struct('A',{A},'B',{B},'M',M,'N',N,'U',U,'p',p-1,...
            'Pfun',@(lam) NLEP.Afun(lam));
    end
end

end % newtonapprox


% ---------------------------------------------------------------------------- %
% Subfunctions
% ---------------------------------------------------------------------------- %

function [a,b,beta] = lejabagby(A,B,C,m,keepA,forceInf)
%LEJABAGBY generate Leja-Bagby points (a,b) on (A,B), with
%  scaling factors beta such that the uniform norm on the control set C is
%  1. Greedy search for a minimum is performed on B. If keepA is true then
%  the points in the output a will be exactly those of A, otherwise the
%  points in a are also chosen via greedy search on A. If forceInf is a
%  positive integeger, the the first forceInf poles in b will be infinity.

if min(abs(B)) < 1e-9
    warning(['LEJABAGBY: There is at least one pole candidate in B being ',...
        'nearby zero. Consider shifting your problem for stability.']);
end

a = zeros(1,m);
a(1) = A(1);
b = zeros(1,m);
if forceInf > 0
    b(1) = inf;
else
    b(1) = B(1);
end

sA = 0*A + 1;
sB = 0*B + 1;
sC = 0*C + 1;

beta = zeros(1,m);
beta(1) = 1;
for j = 1:m-1
    
    sA = sA .* ((A - a(j)) ./ (1 - A/b(j)));
    sC = sC .* ((C - a(j)) ./ (1 - C/b(j)));
    sB = sB .* ((B - a(j)) ./ (1 - B/b(j)));
    
    if keepA
        a(j+1) = A(j+1);
    else
        [~,indA] = max(abs(sA));
        a(j+1) = A(indA);
    end
    [~,indB] = min(abs(sB));
    
    if forceInf > j
        b(j+1) = inf;
    else
        b(j+1) = B(indB);
    end
    beta(j+1) = max(max(abs(sC)));
    
    % treat single point case
    if beta(j+1) < eps
        beta(j+1) = 1;
    end
    
    sA = sA / beta(j+1);
    sB = sB / beta(j+1);
    sC = sC / beta(j+1);
    
end

end % lejabagby


function D = ratnewtoncoeffsm(fm,sigma,xi,beta)
% Compute rational divided differences for the SCALAR function fun, using
% matrix functions. fm has to be a handle to a matrix function fm(A).

m = length(sigma)-1;

sigma = sigma(:);
xi = xi(:); xi(m+1) = NaN; % not used
beta = beta(:);

% build Hessenberg matrices
K = full(spdiags([ [beta(2:m+1)./xi(1:m);0], ones(m+1,1) ],-1:0,m+1,m+1));
H = full(spdiags([ [beta(2:m+1);0], sigma(1:m+1) ],-1:0,m+1,m+1));

K = K.';
H = H.';

D = eye(1,m+1)*fm(K\H)*beta(1);
D = D.';
D = num2cell(D);

end % ratnewtoncoeffsm


function D = ratnewtoncoeffs(fun,sigma,xi,beta)
% Compute rational divided differences for the function fun (can be matrix
% valued), using differencing. The sigma's need to be distinct. For scalar
% functions or non-distinct sigma's it may be better to use
% ratnewtoncoeffsm.

m = length(sigma);
D = cell(1,m);

% compute divided differences D0,D1,...,Dm
D{1} = fun(sigma(1))*beta(1);
n = size(D{1},1);
for j = 2:m
    
    % evaluate current linearizaion at sigma(j);
    if issparse(D{1})
        Qj = spalloc(n,n,nnz(D{1}));
    else
        Qj = 0;
    end
    for k = 1:j-1
        Qj = Qj + D{k}*evalrat(sigma(1:k-1),xi(1:k-1),beta(1:k),sigma(j));
    end
    
    % get divided difference from recursion (could be done via Horner)
    D{j} = (fun(sigma(j)) - Qj)/...
        evalrat(sigma(1:j-1),xi(1:j-1),beta(1:j),sigma(j));
    
end

end % ratnewtoncoeffs


function r = evalrat(sigma,xi,beta,z)
%EVALRAT Evaluate nodal rational function at the points z

r = ones(size(z))/beta(1);
for j = 1:length(sigma)
    r = r .* (z - sigma(j)) ./ (1 - z/xi(j)) / beta(j+1);
end

end % evalrat
