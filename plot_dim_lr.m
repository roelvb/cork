function plot_dim_lr(J,R1,R2,m,d1,d2)

Jk = length(J);
R1k = length(R1);
R2k = length(R2);
k = max([Jk,R1k,R2k]);

plot((0:Jk-1)',J,'o:k'); hold on;
plot((0:R1k-1)',R1,'*:b');
plot((0:R2k-1)',R2,'d:b');
plot([0,k-1],[d1+m,d1+m],'--m');
legend('j','r1','r2','m+d1','Location','NorthWest');
title(['d = ',num2str(d1+d2)]);
xlabel('iteration');
ylabel('dimension');

end
