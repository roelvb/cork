function [mem_rks,mem_cork] = plot_mem(J,R,n,d,plotfun)

if nargin < 5
    plotfun = @plot;
end

nb = length(J);

% RKS
M_rks = zeros(nb,1);
if ~isempty(d)
    for i = 1:nb
        M_rks(i) = n*d*J(i);
    end
else
    d1 = 0;
    d2 = 2;
    M_rks(1) = n;
    for i = 2:nb
        if J(i) < J(i-1)
            d1 = i - 1;
            d2 = 1;
        end
        M_rks(i) = n*d1*J(i) + n*d2*(d2+1)/2;
        d2 = d2 + 1;
    end
end

% CORK
M_cork = zeros(nb,1);
if ~isempty(d)
    for i = 1:nb
        M_cork(i) = n*R(i) + R(i)*J(i)*d;
    end
else
    d1 = 0;
    d2 = 2;
    M_cork(1) = n + 1;
    for i = 2:nb
        if J(i) < J(i-1)
            d1 = i - 1;
            d2 = 1;
        end
        M_cork(i) = n*R(i) + R(i)*d1*J(i) + R(i)*d2*(d2+1)/2;
        d2 = d2 + 1;
    end
end

c = 8/1e6;
mem_rks = c*M_rks;
mem_cork = c*M_cork;

plotfun((0:nb-1)',mem_rks,'o:k'); hold on;
plotfun((0:nb-1)',mem_cork,'+:b');
xlabel('iteration');
ylabel('memory (MB)');
legend('RKS','CORK','Location','NorthWest');

end
