% run_dde_norest

dde = load('dde5000.mat');
n = dde.n;
d = 200;

% matrices
A = cell(1,d+1);
B = cell(1,d+1);
for i = 1:d+1
    A{i} = dde.A0 + cos((i-1)*acos(-1))*dde.A1;
end
for i = 1:d+1
    B{i} = speye(n);
end
M = [zeros(d,1),eye(d)];
diag0 = [2,1./(2:d)];
diag2 = -1./(1:d-1);
N = dde.tau/4*[diag(diag0),zeros(d,1)] + ...
    dde.tau/4*[zeros(d,2),[diag(diag2);zeros(1,d-1)]];

% solve via cork
L = struct('A',{A},'B',{B},'M',M,'N',N);
k = 20;
m = 200;
minit = 50;
maxrest = 0;
rng(0); v0 = randn(n,1);
tolres = 1e-12;
funres = @(Lambda,X) dde_residual(Lambda,X,dde.A0,dde.A1,dde.tau);
shift = 0;
opts = struct('m',m,'v0',v0,'tolres',tolres,'funres',funres,...
    'minit',minit,'maxrest',maxrest,'verbose',1);
[X,lambda,res,flag,info] = cork(L,k,shift,opts);

% plot
subplot(3,1,1); hold off;
[ResC,LamC,R] = plot_conv_eig(info.Res,info.Lam,info.J,tolres);
ylim([1e-18,1e0]);
title('dde 5000 problem');

subplot(3,1,2); hold off;
plot_dim(info.J,info.R,info.m,[]);

subplot(3,1,3); hold off;
[mem_rks,mem_cork] = plot_mem(info.J,info.R,n,[]);
